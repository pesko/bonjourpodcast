class AddToday3ToPodcasts < ActiveRecord::Migration[5.1]
  def change
    add_column :podcasts, :today, :boolean, default: false
  end
end
