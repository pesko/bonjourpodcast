class AddAlreadyuseToPodcasts < ActiveRecord::Migration[5.1]
  def change
    add_column :podcasts, :alreadyuse, :boolean, default: false
  end
end
