class ConvertTodayFromDatetodaysIntoInteger < ActiveRecord::Migration[5.1]
  def change
  change_column :datetodays, :today, 'integer USING CAST(today AS integer)'
  end
end
