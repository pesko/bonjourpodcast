document.addEventListener('readystatechange', function(event) {
    if (event.target.readyState === "loading") {
        initLoader();
    } else if (event.target.readyState === "interactive") {
        dropLoader();
    }
})

function initLoader() {
    const loader = document.getElementById('loader')
    loader.classList.add('donut')
}

function dropLoader() {
    const loader = document.getElementById('loader')
    loader.classList.remove('donut')
}