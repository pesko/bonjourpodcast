require 'date'

class PagesController < ApplicationController
  def home
    date
    is_this_the_same_day
  end


  private

  def is_this_the_same_day
    podcast = Podcast.all
    @today = Date.today.wday
    last_day_in_db = Datetoday.last

    if last_day_in_db.today == @today
      # Selectionne le dernier podcast "true"
      @podcast_of_the_day = Podcast.where(today: true).last
    else
      podcasts = podcast.where(today: false)
      @podcast_of_the_day = podcasts.sample
      @podcast_of_the_day.today = true
      @podcast_of_the_day.save

      # Changement de date du jour
      save_today = Datetoday.new(today: @today)
      save_today.save!
    end
  end

  def date
    # Afficher le jour de la semaine en français
    date_today = Date.today.wday
    date_array = [
      {Dimanche: 0},
      {Lundi: 1},
      {Mardi: 2},
      {Mercredi: 3},
      {Jeudi: 4},
      {Vendredi: 5},
      {Samedi: 6}
    ]
    date_array.each do |element|
      element.each do |key, value|
        if date_today == value
          @today_week = key
        end
      end
    end
    
    #Afficher le numéro de la journée
    d = Date.today
    @day = d.strftime("%d")

    #Afficher le mois en français
    month = d.strftime("%m")
    months_array = [
      {Janvier: "01"},
      {Fevrier: "02"},
      {Mars: "03"},
      {Avril: "04"},
      {Mai: "05"},
      {Juin: "06"},
      {Juillet: "07"},
      {Aout: "08"},
      {Septembre: "09"},
      {Octobre: "10"},
      {Novembre: "11"},
      {Décembre: "12"}
    ]
    months_array.each do |element|
      element.each do |key, value|
        if month == value
          @month = key
        end
      end
    end
  end
end
