puts "Cleaning database..."
Podcast.destroy_all
puts "Done"
puts ".........."

puts "Generate podcasts..."

#01 diffuse le 09 mars
podcast = Podcast.new
podcast.title = "Riviera Détente #22 - Croustiviandes"
podcast.html = '<iframe width="100%" height="300" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/307790762&color=%2394c1ad&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true&visual=true"></iframe>'
podcast.today = true
podcast.save!

podcast = Podcast.new
podcast.title = "Qualiter Studio404 #57 - Février 2018 : Chine, Cloud, Mail et Prisons"
podcast.html = '<iframe width="100%" height="300" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/405669630&color=%2394c1ad&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true&visual=true"></iframe>'
podcast.save!

podcast = Podcast.new
podcast.title = "Louie Media #1 Comment savoir si vous avez quitté l'enfance"
podcast.html = '<iframe width="100%" height="300" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/409647048&color=%2394c1ad&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true&visual=true"></iframe>'
podcast.save!

podcast = Podcast.new
podcast.title = "FloodCast S03E11 - Joyeux Anniversaire Suricate"
podcast.html = '<iframe width="100%" height="300" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/405212550&color=%2394c1ad&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true&visual=true"></iframe>'
podcast.save!

podcast = Podcast.new
podcast.title = "Transfert S02E18 - Le désir d'être mère"
podcast.html = '<iframe width="100%" height="300" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/408987615&color=%2394c1ad&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true&visual=true"></iframe>'
podcast.save!

podcast = Podcast.new
podcast.title = "2 heures de perdues, Judge Dredd"
podcast.html = '<iframe width="100%" height="300" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/226075540&color=%2394c1ad&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true&visual=true"></iframe>'
podcast.save!

podcast = Podcast.new
podcast.title = "Nouvelle École #57 - Kyan Khojandi : Le temps est ton ami"
podcast.html = '<iframe width="100%" height="300" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/405460482&color=%2394c1ad&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true&visual=true"></iframe>'
podcast.save!

podcast = Podcast.new
podcast.title = "HS4, L'Apéro du Captain vs ZQSD"
podcast.html = '<iframe width="100%" height="300" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/240478204&color=%2394c1ad&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true&visual=true"></iframe>'
podcast.save!

podcast = Podcast.new
podcast.title = "Louie Media #2 Comment survivre à la rentrée"
podcast.html = '<iframe width="100%" height="300" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/409649457&color=%2394c1ad&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true&visual=true"></iframe>'
podcast.save!

podcast = Podcast.new
podcast.title = "RadioNavo Marché Parlé #9 - La jalousie, ce monstre"
podcast.html = '<iframe width="100%" height="300" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/240987238&color=%2394c1ad&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true&visual=true"></iframe>'
podcast.save!

podcast = Podcast.new
podcast.title = "Version Originale Quiplash & Fin - 6h - La 8ème #NuitOriginale"
podcast.html = '<iframe width="100%" height="300" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/322266879&color=%2394c1ad&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true&visual=true"></iframe>'
podcast.save!

podcast = Podcast.new
podcast.title = "Comedy Corner S01E04 - Spéciale Stand-up - avec Kyan Khojandi, Yacine Belhousse, Adrien Ménielle et FloBer"
podcast.html = '<iframe width="100%" height="300" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/264476049&color=%2394c1ad&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true&visual=true"></iframe>'
podcast.save!

podcast = Podcast.new
podcast.title = "ZQSD.fr #56, le podcast GOTY 2017 en retard"
podcast.html = '<iframe width="100%" height="300" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/400588803&color=%2394c1ad&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true&visual=true"></iframe>'
podcast.save!

podcast = Podcast.new
podcast.title = "Le Cosy Corner #25 - Mentalité aïkido"
podcast.html = '<iframe width="100%" height="300" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/406838007&color=%2394c1ad&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true&visual=true"></iframe>'
podcast.save!

podcast = Podcast.new
podcast.title = "Nouvelle École #58 - Jack Parker : On n'est pas des articles Medium"
podcast.html = '<iframe width="100%" height="300" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/409010529&color=%2394c1ad&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true&visual=true"></iframe>'
podcast.save!

podcast = Podcast.new
podcast.title = "Riviera Détente #37 - Mimosa"
podcast.html = '<iframe width="100%" height="300" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/405449334&color=%2394c1ad&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true&visual=true"></iframe>'
podcast.save!

#17
podcast = Podcast.new
podcast.title = "ZQSD.fr ABCD #17, le podcast de la culture"
podcast.html = '<iframe width="100%" height="300" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/406617366&color=%2394c1ad&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true&visual=true"></iframe>'
podcast.save!



# podcast = Podcast.new
# podcast.title = ""
# podcast.html = ''
# podcast.save!

puts "Done !"