class CreateDatetodays < ActiveRecord::Migration[5.1]
  def change
    create_table :datetodays do |t|
      t.string :today

      t.timestamps
    end
  end
end
